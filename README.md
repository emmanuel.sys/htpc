# htpc

docker-compose file for managing a media center stack :
- Plex : media library, transcoding and remote streaming
- Sonarr : tv series automatic download
- Jackett : proxy for torrent content indexers
- Transmission : torrent downloader
- Tautulli : usage statistics for Plex
- Radarr: movies download
- Bazarr: subtitles automatic downloads for Sonarr/Radarr

# Configuration

Edit `.env` configuration file
- `MOUNT_POINT`: shared volume location for application files and configuration (`/` terminated)
- `MEDIA_PATH`: downloaded media location (`/` terminated)

# Running

Start the stack with `docker-compose up -d` and stop with `docker-compose down`

# Updating

To update the docker images, run `./update.sh`
